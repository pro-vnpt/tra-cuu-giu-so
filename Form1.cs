﻿using DeviceId;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Tra_cuu_giu_so
{
    public partial class Form1 : Form
    {
        HttpClient client = new HttpClient();
        private Random _random = new Random();
        public Form1()
        {
            InitializeComponent();
            readSetting();
        }

        private async void btn_start_Click(object sender, EventArgs e)
        {
            saveSetting();
            btn_start.Enabled = false;
            btn_stop.Enabled = true;
            grid_view.Rows.Clear();
            grid_view.Refresh();
            string[] lines = tb_phones.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );

            Thread y = new Thread(() =>
            {
                for (int i = 0; i < lines.Length; i++)
                {
                    string phone = lines[i];
                    if (btn_stop.Enabled)
                    {
                        Thread.Sleep(int.Parse(tb_delay.Text));
                        GetTTTB(phone.Trim());
                    }
                }
                this.Invoke((MethodInvoker)delegate {
                    btn_start.Enabled = true;
                    btn_stop.Enabled = false;
                });
            });
            y.IsBackground = true;
            y.Start();
        }

        private async void GetTTTB(string sdt)
        {
            string cuoc_ck = "N/A";
            string time_ck = "N/A";
            string ngay_thuc_hien = "N/A";
            string nguoi_thuc_hien = "N/A";
            string ghi_chu = "N/A";
            // get cam kết
            string url = "http://" + tb_host.Text +"/ccbs/main?1iutlomLork=zxgi{{5zxgi{{emo{yu5mkze|gr{kesyoyjt&syoyjt=" + sdt + "&_=" + DateTimeOffset.Now.ToUnixTimeMilliseconds();
            string res = "";
            res = await HttpClientUtil.callAPICCBS(client, HttpMethod.Get, url, "");
            cuoc_ck = getCK(res);
            time_ck = getTimeCK(res);



            // get thông tin chọn số
            url = "http://" + tb_host.Text + "/ccbs/main?1iutlomLork=zxgi{{5zxgi{{emo{yu5gpg~eotjk~&y{hy=" + sdt + "&_=" + DateTimeOffset.Now.ToUnixTimeMilliseconds();
            res = await HttpClientUtil.callAPICCBS(client, HttpMethod.Get, url, "");
            ngay_thuc_hien = getThoiGian(res);
            nguoi_thuc_hien = getNguoiThucHien(res);
            ghi_chu = getGhiChu(res);

            Invoke(new Action(() =>
            {
                grid_view.Rows.Add(sdt, cuoc_ck, time_ck, ngay_thuc_hien, nguoi_thuc_hien, ghi_chu);
            }));

        }



        private string getCK(string data)
        {
            string token = "";
            // @"(?<={from}).*?(?={to})"
            // nếu {to} là 1 dấu "
            // thì phải thêm ""

            //var res = Regex.Matches(data, @"(?<=""access_token"":"").*?(?="")", RegexOptions.Singleline);
            var res = Regex.Matches(data, @"(?<=\("").*?(?="")", RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                token = res[0].ToString();
            }
            return token;
        }

        private string getTimeCK(string data)
        {
            string token = "";
            // @"(?<={from}).*?(?={to})"
            // nếu {to} là 1 dấu "
            // thì phải thêm ""

            //var res = Regex.Matches(data, @"(?<=""access_token"":"").*?(?="")", RegexOptions.Singleline);
            var res = Regex.Matches(data, @"(?<=\("").*?(?="")", RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                token = res[1].ToString();
            }
            return token;
        }

        private string getTenKhachHang(string data)
        {
            return StringUtil.extractFromTo(data, "id=\"tk2_1\">", "</td>", 0);
        }

        private string getNguoiThucHien(string data)
        {
            return StringUtil.extractFromTo(data, "id=\"tk5_1\">", "</td>", 0);
        }

        private string getDiaChi(string data)
        {
            return StringUtil.extractFromTo(data, "id=\"tk3_1\">", "</td>", 0);
        }

        private string getThoiGian(string data)
        {
            return StringUtil.extractFromTo(data, "id=\"tk4_1\">", "</td>", 0);
        }

        private string getGhiChu(string data)
        {
            return StringUtil.extractFromTo(data, "<td align=\"center\">", "</td>", 1);
        }


        public void readSetting()
        {
            string fileName = @"setting.txt";
            string jsonString = "";
            try
            {
                jsonString = File.ReadAllText(fileName);
            }
            catch (Exception ex)
            {

            }
            if (string.IsNullOrEmpty(jsonString)) return;
            string[] data = jsonString.Split('-');
            tb_username.Text = data[0];
            tb_password.Text = data[1];
            tb_otp.Text = data[2];
            tb_delay.Text = data[3];
            tb_host.Text = data[4];
        }

        public void saveSetting()
        {
            string json = string.Format("{0}-{1}-{2}-{3}-{4}", tb_username.Text, tb_password.Text, tb_otp.Text, tb_delay.Text, tb_host.Text);
            File.WriteAllText("setting.txt", json);
        }

        public class AppSetting
        {
            public string username { get; set; }
            public string password { get; set; }
            public string otp { get; set; }
            public string delay { get; set; }
            public string host { get; set; }
        }

        private async void btn_login_Click(object sender, EventArgs e)
        {
            string username = tb_username.Text;
            string password = tb_password.Text;
            string otp = tb_otp.Text;
            saveSetting();
            string request = "1iutlomLork=gjsot5pl{tizout&1pl{tizout=tku4ysgxz{o4rg\u007fu{z4ykz[ykxVgxgskzkx./&username=" + username + "&password=" + password + "&options=" + otp;
            var res = await HttpClientUtil.callAPILoginCCBS(client, HttpMethod.Post, "http://"+ tb_host.Text +"/ccbs/main", request);
            if (res.Contains("0"))
            {
                btn_login.Enabled = false;
                tb_username.Enabled = false;
                tb_password.Enabled = false;
                tb_otp.Enabled = false;
            }
            else
            {
                MessageBox.Show("Lỗi đăng nhập");
            }
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            btn_start.Enabled = true;
            btn_stop.Enabled = false;
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            string deviceId = new DeviceIdBuilder()
            .OnWindows(windows => windows
                .AddProcessorId()
                .AddMotherboardSerialNumber()
                .AddSystemDriveSerialNumber())
            .ToString();
            File.WriteAllText("key.txt", deviceId);
            HttpClient client = new HttpClient();
            string content = $@"{{
                                          ""app"": ""tra_cuu_giu_so"",
                                          ""key"": ""{deviceId}""
                                        }}";
            var res = await HttpClientUtil.callAPI(client, HttpMethod.Post, "http://xacthuc.provnpt.com/api/v1/auth-app", content);
            if (res.Contains("\"active\":true"))
            {
                return;
            }
            else
            {
                MessageBox.Show("App chưa được kích hoạt");
                this.Close();
            }
        }
    }
}
